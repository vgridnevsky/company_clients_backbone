#!/bin/sh
# Doc: https://aiohttp-wsgi.readthedocs.io/en/stable/wsgi.html
aiohttp-wsgi-serve company_clients.wsgi:application \
                   --static /static=./static \
                   --host=127.0.0.1 \
                   --threads 10
