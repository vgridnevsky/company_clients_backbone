"""
Models for Client catalog app.
"""


from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.tz import tzlocal
from pytz import utc

from django.db import models


class Client(models.Model):
    """
    Contains all required client data.
    """
    first_name = models.CharField('First name', max_length=50)
    last_name = models.CharField('Last name', max_length=50)
    birth_date = models.DateField('Birth date')
    photo = models.ImageField('Photo', upload_to="uploads/%Y/%m/%d/")
    points = models.IntegerField('Points', default=0)

    def calculate_age(self):
        """
        Converts age to birth date. Timezone-aware operation.
        """
        # Convert timezone-unaware datetime object to UTC datetime
        birth_date = utc.localize(
            datetime.combine(self.birth_date, datetime.min.time())
        )
        # Create "now" object with local tz
        local_timezone_now = datetime.now(tzlocal())
        # Create timezone-relative datetime delta
        rdelta = relativedelta(birth_date, local_timezone_now).normalized()
        # Return years from relativedelta
        return abs(rdelta.years)

    def __str__(self):
        return(
            "{} {}. {}\n{}".format(
                self.first_name,
                self.last_name,
                self.birth_date.strftime('%b %d %Y'),
                self.photo)
        )
