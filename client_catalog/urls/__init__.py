"""
Contains url routing for client catalog app.
"""

from django.conf.urls import include, url

from client_catalog.views import IndexView, ReturnRedirectView


urlpatterns = [
    # Index
    url(r'^$', IndexView.as_view()),
    # API
    url(r'^api/', include('client_catalog.urls.api')),
    # Redirect for Backbone.router routes
    url(r'^(.*)$', ReturnRedirectView.as_view())
]
