"""
Urls for client catalog app API.
"""


from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns

from client_catalog.views.api import ClientList, ClientDetail, ClientVote, \
                                     ApiRoot


urlpatterns = [
    url(r'^$', ApiRoot.as_view()),
    url(r'^clients/$', ClientList.as_view(), name='client-list'),
    url(r'^clients/(?P<pk>\d+)/$', ClientDetail.as_view(),
        name='client-detail'),
    url(r'^clients/(?P<pk>\d+)/vote/$', ClientVote.as_view(),
        name='client-vote')
]

# Format suffixes
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])
