var app_router = Backbone.Router.extend({
    routes: {
        '': 'home',
        'clients': 'client_list',
        'client/new': 'add_client',
        'client/:client_id': 'client'
    },

    initialize: function() {
        this.activeView = null;
    },

    home: function() {
        this.clearPage('.nav-link:contains("Home")');

        let homeViewInstance = new HomeView();
        homeViewInstance.render();
        $('.page-content').html('').append(homeViewInstance.$el);

        this.activeView = homeViewInstance;
    },

    clearPage: function(navLinkSelector) {
        // Destroy active Backbone view
        if (this.activeView != null) {
            this.activeView.destroy();
        }
        // Disable Booststrap nav-link highlight
        $('.navbar').find('.nav-link').removeClass('active');
        // Set new highlighted link
        if (navLinkSelector != null) {
            $('.navbar').find(navLinkSelector).addClass('active');
        }
        // Show preloader
        $('.page-content').html(`
            <div class="text-center">
              <img src="/static/img/preloader.gif">
            </div>
        `);
    },

    client_list: function() {
        this.clearPage('.nav-link:contains("Client catalog")');

        // Initialize the view
        let clientCardGroupInstance = new ClientCardGroup();
        $('.page-content').html('').append(clientCardGroupInstance.$el);
        clientCardGroupInstance.on('collectionFetchSuccess', function() {
            clientCardGroupInstance.render();
        });

        this.activeView = clientCardGroupInstance;
    },

    client: function(client_id) {
        this.clearPage();

        // Initialize the model
        var current_model = new ClientModel({
            'id': client_id
        });
        current_model.fetch({
            'success': function() {
                // Initialize the view
                let clientDetailsInstance = new ClientDetails({
                    model: current_model
                });
                clientDetailsInstance.render();
                $('.page-content').html('').append(clientDetailsInstance.$el);
            }
        });

        this.activeView = clientDetailsInstance;
    },

});

var start_router = function() {
    // Create application router instance to pass urls
    var app_router_instance = new app_router();
    // Pass app-urls to application router, let other
    // work as usual
    window.document.addEventListener('mouseup', function(e) {
        // Get target
        e = e || window.event;
        var target = e.target || e.srcElement;
        // Check it's left mouse button
        if (e.which == 1) {
            if (target.nodeName.toLowerCase() === 'a') {
                e.preventDefault();
                var uri = target.getAttribute('href');
                if (uri !== null) {
                    if (uri.startsWith('http://') || uri.startsWith('www.')) {
                        window.open(uri, '_newtab');
                    } else {
                        app_router_instance.navigate(uri, {
                            trigger: true,
                            replace: false
                        });
                    }
                }
            }
        }
    });
    // Init Backbone history module
    Backbone.history.start({
        pushState: true
    });
};

$(function() {
    start_router();
});
