var templates = {
    'clientCard': _.template(`
        <div class="card-header">
            <a href="#client/<%=id%>" class="text-white">
                <%= first_name %> <%= last_name %>
            </a>
        </div>

        <div class="card-body"><img src="<%=photo%>" alt=""/></div>

        <div class="card-footer">
            <div class="client_card_vote">
                <div class="input-group">
                <span class="input-group-addon bg-dark text-muted border-secondary vote_count">
                    <%=points%>
                </span>
                <span class="input-group-btn">
                    <button class="btn btn-primary border-secondary vote" type="button">
                        <i class="fa fa-thumbs-up"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>
    `),
    'clientDetails': _.template(`
        <h3>Client details: <%=first_name%> <%=last_name%></h3>

        <table class="table">
          <tbody>
            <tr>
              <td>First name</td> <td><%=first_name%></td>
            </tr>
            <tr>
              <td>Last name</td> <td><%=last_name%></td>
            </tr>
            <tr>
              <td>Age</td> <td><%=age%></td>
            </tr>
            <tr>
              <td>Birth date</td> <td><%=birth_date%></td>
            </tr>
            <tr>
              <td>Photo</td>
              <td>
                <a href="<%=photo%>">
                  <img src="<%=photo%>" alt="" width="200px">
                </a>
              </td>
            </tr>
          </tbody>
        </table>
    `),
    'homeView': `
        <div class="jumbotron bg-dark text-muted">
            <h1 class="text-white">Home</h1>
            <p class="lead">
                This app is an example, using Backbone routing,
                Django Rest Framework and Boostrap 4.
            </p>

            <hr/>

            <b>Admin:</b>
            <ul>
                <li>Login: <code>Wis</code></li>
                <li>Pass: <code>Lipton_Lime</code></li>
            </ul>

            <b>Postgres parameters:</b>
            <ul>
                <li>Database: <code>company_clients</code></li>
                <li>Login role: <code>company_clients_demo_user</code></li>
                <li>Login role password: <code>foobar</code></li>
            </ul>

            <a
                class="btn btn-lg btn-primary"
                href="#clients"
                role="button"
            >Continue »</a>
        </div>`
};

var ClientModel = Backbone.Model.extend({
    urlRoot: '/api/clients',
    // Add trailing slash
    url: function() {
        var origUrl = Backbone.Model.prototype.url.call(this);
        return origUrl += origUrl.endsWith('/') ? '' : '/';
    },
    upvote: function() {
        let self = this;
        var voting_url = this.url() + 'vote/';
        $.ajax({
            url: voting_url,
            method: 'PUT',
            success: function(result) {
                self.trigger('vote-success', result);
            },
            error: function() {
                self.trigger('vote-failure');
            }
        });
    }
});

var ClientCollection = Backbone.Collection.extend({
    model: ClientModel,
    url: '/api/clients'
});

var ClientCard = Backbone.View.extend({
    tagName: 'article',
    className: 'card text-center text-muted bg-dark client_card',
    template: templates.clientCard,
    events: {
        'click button.vote': 'vote',
    },
    vote: function() {
        this.model.upvote();
    },
    show_vote_success: function(server_output) {
        // Update points
        if ('points' in server_output) {
            this.$el.find('.vote_count').html(server_output.points);
        }
        // Show success with button
        var vote_button = this.$el.find('button.vote');
        vote_button.removeClass('btn-primary').removeClass('btn-warning');
        vote_button.addClass('btn-success');
        setTimeout(function() {
            vote_button.removeClass('btn-success').removeClass('btn-warning');
            vote_button.addClass('btn-primary');
        }, 500);
    },
    show_vote_failure: function() {
        var vote_button = this.$el.find('button.vote');
        vote_button.removeClass('btn-primary').removeClass('btn-success');
        vote_button.addClass('btn-warning');
        setTimeout(function() {
            vote_button.removeClass('btn-warning').removeClass('btn-success');
            vote_button.addClass('btn-primary');
        }, 500);
    },
    initialize: function(options) {
        this.model = options.model;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'vote-success', this.show_vote_success);
        this.listenTo(this.model, 'vote-failure', this.show_vote_failure);
    },
    render: function() {
        let html = this.template(this.model.attributes);
        // Clear view HTML and render again
        this.$el.html('').append(html);
        // Reattach view events
        this.delegateEvents();
    },
    destroy: function() {
        // Unbind the view
        this.undelegateEvents();
        this.$el.removeData().unbind();
        // Remove view from DOM
        this.remove();
        Backbone.View.prototype.remove.call(this);
    }
});

var ClientCardGroup = Backbone.View.extend({
    tagClass: 'row',
    initialize: function(options) {
        var self = this;
        // Init collection, used by the view
        this.collection = new ClientCollection();
        this.collection.fetch({
            'success': function() {
                self.trigger('collectionFetchSuccess');
            },
            'error': function() {
                self.trigger('collectionFetchError');
            }
        });
        // Client card instances
        this.cards = [];
    },
    renderItem: function(item) {
        let clientCardInstance = new ClientCard({
            model: item
        });
        clientCardInstance.render();
        this.$el.append(clientCardInstance.$el);
    },
    render: function() {
        this.collection.forEach(this.renderItem, this);
    },
    destroy: function() {
        // Unbind the view
        this.undelegateEvents();
        this.$el.removeData().unbind();
        // Remove view from DOM
        this.remove();
        Backbone.View.prototype.remove.call(this);
    }
});

var ClientDetails = Backbone.View.extend({
    className: 'jumbotron bg-dark text-white',
    template: templates.clientDetails,
    initialize: function(options) {
        this.model = options.model;
    },
    render: function() {
        let html = this.template(this.model.attributes);
        // Clear view HTML and render again
        this.$el.html('').append(html);
    },
    destroy: function() {
        // Unbind the view
        this.undelegateEvents();
        this.$el.removeData().unbind();
        // Remove view from DOM
        this.remove();
        Backbone.View.prototype.remove.call(this);
    }
});

var HomeView = Backbone.View.extend({
    template: templates.homeView,
    initialize: function() {},
    render: function() {
        this.$el.append(this.template);
    },
    destroy: function() {
        // Unbind the view
        this.undelegateEvents();
        this.$el.removeData().unbind();
        // Remove view from DOM
        this.remove();
        Backbone.View.prototype.remove.call(this);
    }
});
