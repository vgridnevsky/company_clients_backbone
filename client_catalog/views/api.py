"""
Contains site API.
ClientList and ClientDetail classes allow to
access client info.
ClientVote allows to vote for a client.
"""

from django.db import transaction
from django.db.models import F
from django.utils.decorators import method_decorator
from rest_framework.views import APIView
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status

from client_catalog.models import Client
from client_catalog.serializers import ClientSerializer


MAX_VOTE_POINTS = 10


class ApiRoot(APIView):
    """
    The entry endpoint of our API.
    """

    def get(self, request):
        """
        Returns index page
        """
        return Response({
            'clients': reverse('client-list', request=request),
        })


class ClientDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single client.
    """

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of clients.
    """

    serializer_class = ClientSerializer
    queryset = Client.objects.all().order_by('id')


class ClientVote(APIView):
    """
    Accepts vote for a client.
    """

    def put(self, request, pk):
        """
        Receives client ID, increments client votes.
        """

        # Use F-expression for update
        with transaction.atomic():
            Client.objects.select_for_update().filter(
                pk=pk,
                points__lt=MAX_VOTE_POINTS
            ).update(points=F('points') + 1)
        # Send updated points in user request
        client = Client.objects.get(pk=pk)
        points = client.points

        return Response(
            {'points': points},
            status=status.HTTP_200_OK
        )
