"""
Views for the client catalog: list, creation and deletion, etc.
"""


from django.views.generic import TemplateView
from django.views import View
from django.shortcuts import redirect


class IndexView(TemplateView):
    """
    This view simply returns index page.
    """
    template_name = 'index.html'

class ReturnRedirectView(View):
    """
    This view redirects user to Backbone-style URL.
    """
    def get(self, request, *args, **kwargs):
        """
        Redirects to the url with leading hash sign,
        which is fed to the Backbone.
        """
        redirect_path = "/#{}".format(request.path.split("#")[0])
        return redirect(redirect_path)
