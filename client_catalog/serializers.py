"""Django rest framework serializers to use with client catalog models."""


from client_catalog.models import Client
from rest_framework import serializers


class ClientSerializer(serializers.HyperlinkedModelSerializer):

    """
    Serializer for a client.
    """

    age = serializers.SerializerMethodField()
    photo = serializers.ImageField(use_url=True)

    def get_age(self, model):
        return model.calculate_age()

    class Meta:
        model = Client
        fields = ('id', 'first_name', 'last_name', 'birth_date', 'photo',
                  'points', 'age')


class ClientVoteSerializer(serializers.HyperlinkedModelSerializer):

    """
    Serializer for a client vote.
    """

    class Meta:
        model = Client
        fields = ('pk', 'points')
