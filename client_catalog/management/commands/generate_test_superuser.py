"""
Custom manage.py command to generate test superuser.
"""

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

SUPERUSER_NAME = "Wis"
SUPERUSER_MAIL = "admin@wis.eu.com"
SUPERUSER_PASS = "Lipton_Lime"


class Command(BaseCommand):
    """
    Management command, used to add test superuser.
    """

    help = 'Adds test superuser: {} / {}.'.format(
        SUPERUSER_NAME,
        SUPERUSER_PASS
    )

    def handle(self, *args, **options):
        User.objects.create_superuser(
            SUPERUSER_NAME,
            SUPERUSER_MAIL,
            SUPERUSER_PASS
        )
        self.stdout.write(
            self.style.SUCCESS(
                'Successfully added '
                '{} / {}'.format(SUPERUSER_NAME, SUPERUSER_PASS)
            )
        )
