"""
Custom manage.py command to generate new clients.
"""

from os import path, listdir, pardir
from random import randrange
from datetime import datetime, timedelta

from django.core.files import File
from django.core.management.base import BaseCommand
from django.conf import settings
import names

from client_catalog.models import Client


MIN_BIRTHDAY_DATETIME_STR = 'Jan 1 1975  1:00AM'
MAX_BIRTHDAY_DATETIME_STR = 'Dec 30 1995  11:55PM'
PHOTO_DIRECTORY = path.join(
    path.join(settings.BASE_DIR, pardir),
    'res/gen_photos'
)


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


class Command(BaseCommand):
    """
    Management command, used to add random client records.
    """

    help = 'Adds random client records for testing'

    def handle(self, *args, **options):
        client_count = 0
        # Set min and max dates
        min_datetime_obj = datetime.strptime(MIN_BIRTHDAY_DATETIME_STR,
                                             '%b %d %Y %I:%M%p')
        max_datetime_obj = datetime.strptime(MAX_BIRTHDAY_DATETIME_STR,
                                             '%b %d %Y %I:%M%p')
        # Save and try to build clients
        for filename in listdir(PHOTO_DIRECTORY):
            photo_path = path.join(PHOTO_DIRECTORY, filename)
            try:
                photo_file_object = File(open(photo_path, 'rb'))
            except IOError:
                continue
            new_client = Client.objects.create(
               first_name=names.get_first_name(),
               last_name=names.get_last_name(),
               birth_date=random_date(
                   min_datetime_obj,
                   max_datetime_obj
               )
            )
            photo_name = path.basename(photo_path)

            new_client.photo.save(
                photo_name, photo_file_object, save=False
            )
            new_client.save()
            client_count += 1
        self.stdout.write(
            self.style.SUCCESS(
                'Successfully generated '
                '{} client records.'.format(client_count)
            )
        )
